/* 
 * File:   serverProtoImplem.h
 * Author: szczepanczykd
 *
 * Created on April 10, 2016, 7:49 PM
 */

#ifndef SERVERPROTOIMPLEM_H
#define	SERVERPROTOIMPLEM_H

#include "serverController.h"
#include "serverProto.h"
#include <cstring>
#include <vector>
#include <sstream>

#include <memory>

using namespace std;

class serverProtoImplem {
    
    
public:
    serverProtoImplem();
    virtual ~serverProtoImplem();
    static vector <serverProto*> _proto_list;
    static void addProto(serverProto &proto);
    static void useProto(string protoName,std::vector <serverClient> *&clients, serverClient *sender, const std::string message);
    static auto serverProtoFactory(string protoName) -> unique_ptr <serverProto>;
    
private:

};

class serverProtoDir : public serverProto
{
    string _protoName;
public:
    serverProtoDir(string protoName);
    void sendProtoMessage(std::vector <serverClient> *&clients,serverClient *sender,const std::string message);
    string getProtoName();
};

class serverProtoAll : public serverProto
{
    string _protoName;
    public:
    serverProtoAll(string protoName);
    void sendProtoMessage(std::vector <serverClient> *&clients,serverClient *sender,const std::string message);
    string getProtoName(); 
};

class serverProtoGetusrs : public serverProto
{
    string _protoName;
    public:
        serverProtoGetusrs(string protoName);
    void sendProtoMessage(std::vector <serverClient> *&clients,serverClient *sender,const std::string message);
    string getProtoName();   
};


#endif	/* SERVERPROTOIMPLEM_H */

