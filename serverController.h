/* 
 * File:   serverController.h
 * Author: szczepanczykd
 *
 * Created on April 10, 2016, 1:53 PM
 */

#ifndef SERVERCONTROLLER_H
#define	SERVERCONTROLLER_H

#include "serverControllerInterface.h"
#include "serverClient.h"
#include "log.h"
#include "fileRaii.h"
#include <fstream>
#include <sstream>
#include "serverProto.h"
#include "serverProtoImplem.h"

#include <memory>

class serverController : public serverControllerInt {
public:
    serverController();
    serverController(const serverController& orig);
    virtual ~serverController();
    
    static int sendMessage(std::string text,serverClient & client);
    std::string reciveMessage();

    std::string parseMessage(std::string text);
    void loopped_task(std::vector <serverClient> *&clients);
    void kill(std::vector <serverClient> *&clients);
private:
    serverClient _client;
    
};

#endif	/* SERVERCONTROLLER_H */

