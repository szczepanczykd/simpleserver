/* 
 * File:   serverSocket.h
 * Author: szczepanczykd
 *
 * Created on April 8, 2016, 7:28 PM
 */

#ifndef SERVERSOCKET_H
#define	SERVERSOCKET_H

#include <iostream>
#include <cstring>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "server.h"

using namespace std;

class serverSocket : public server {
    
struct sockaddr_in _addrport;
int _status;
int _cnctsocket;
int _connection_port;
bool _state;
void init(string ip, int number);
public:
    serverSocket();
    serverSocket(string ip, int number);
    virtual ~serverSocket();
    
    void startListening(string ip, int number);
    void kill();
    int getStatus();
    int& getConnectSocket();
    int getConnection_port();
    struct sockaddr_in getAddrport();
    
private:

};

#endif	/* SERVERSOCKET_H */

