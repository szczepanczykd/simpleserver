/* 
 * File:   serverControllerInterface.h
 * Author: szczepanczykd
 *
 * Created on April 10, 2016, 1:36 PM
 */

#ifndef SERVERCONTROLLERINTERFACE_H
#define	SERVERCONTROLLERINTERFACE_H

#include <iostream>
#include <cstring>
#include <vector>
#include "serverClient.h"
#include "serverSocket.h"

using namespace std;

class serverControllerInt {
public:
    virtual std::string reciveMessage() = 0;
    virtual std::string parseMessage(std::string text) = 0;
    virtual void loopped_task(std::vector <serverClient> *&clients) = 0;
};

#endif	/* SERVERCONTROLLERINTERFACE_H */

