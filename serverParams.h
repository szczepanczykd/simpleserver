/* 
 * File:   serverParams.h
 * Author: root
 *
 * Created on April 8, 2016, 6:58 PM
 */

#ifndef SERVERPARAMS_H
#define	SERVERPARAMS_H

#include <cstring>

struct serverParams{
  std::string ip;
  int port;
};

#endif	/* SERWERPARAMS_H */

