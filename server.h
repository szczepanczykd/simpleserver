/* 
 * File:   server.h
 * Author: szczepanczykd
 *
 * Created on April 10, 2016, 1:17 PM
 */

#ifndef SERVER_H
#define	SERVER_H

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>

class server{
public:
    virtual struct sockaddr_in getAddrport() = 0;
    virtual int& getConnectSocket() =0;
    virtual void kill() = 0;
};

#endif	/* SERVER_H */

