/* 
 * File:   serverSocket.cpp
 * Author: szczepanczykd
 * 
 * Created on April 8, 2016, 7:28 PM
 */

#include "serverSocket.h"

serverSocket::serverSocket()
{
    _state = false;
    _status = 0;
    _cnctsocket = 0;
    _connection_port = 0;
}


serverSocket::serverSocket(string ip, int number) {

    this->init(ip,number);
    
}

serverSocket::~serverSocket() {
}


void serverSocket::startListening(string ip, int number)
{
    if(!_state)
    {
        this->init(ip,number);
    }
    else
    {
        std::cout << "Already alive!\n";
    }
}

    int serverSocket::getStatus()
    {
      return _status;
    }
    int& serverSocket::getConnectSocket()
    {
     return _cnctsocket;
    }
    int serverSocket::getConnection_port()
    {
       return _connection_port;
    }
    struct sockaddr_in serverSocket::getAddrport()
    {
       return _addrport;
    }
    
    
    void serverSocket::init(string ip, int number)
    {
        _state = true;   
        _cnctsocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if(_cnctsocket == -1)
        {
        cout << "Cannot create socket!\n";
        exit(2);
        }

        _addrport.sin_port = htons(number);
        _addrport.sin_family = AF_INET;
        _addrport.sin_addr.s_addr = inet_addr(ip.c_str());

        _status = bind(_cnctsocket, (struct sockaddr*) &_addrport, sizeof(_addrport));

        if(_status == -1)
        {
        cout << "Cannot bind!\n";
        close(_cnctsocket);
        exit(2);
        }

        _status = listen(_cnctsocket,5);

        if(_status == -1)
        {
        cout << "Cannot listen on socket!\n";
        close(_cnctsocket);
        exit(2);
        }
        cout << "server socket created successfully!\n";
    }
    
    
    void serverSocket::kill()
    {
        if(_cnctsocket)
            close(_cnctsocket);
        
    _state = false;
    _status = 0;
    _cnctsocket = 0;
    _connection_port = 0;
        
    }