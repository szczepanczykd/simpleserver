/* 
 * File:   fileRaii.h
 * Author: szczepanczykd
 *
 * Created on April 9, 2016, 10:46 PM
 */

#ifndef FILERAII_H
#define	FILERAII_H

#include <iostream>
#include <fstream>


class fileRaii {
    std::ofstream *__file;
public:
    fileRaii(std::ofstream &file);
    virtual ~fileRaii();
    template <class Type> void writeText(Type text)
    {
        *__file << text;
        __file->flush();
    }
    bool is_open();
private:

};

#endif	/* FILERAII_H */

