/* 
 * File:   serverProto.h
 * Author: szczepanczykd
 *
 * Created on April 10, 2016, 5:59 PM
 */

#ifndef SERVERPROTO_H
#define	SERVERPROTO_H

#include <vector>
#include <iostream>
#include <cstring>
#include "serverClient.h"

using namespace std;

class serverProto {
   
public:
    virtual void sendProtoMessage(std::vector <serverClient> *&clients,serverClient *sender,const std::string message) = 0;
    virtual string getProtoName() = 0;

};

#endif	/* SERVERPROTO_H */

