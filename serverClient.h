/* 
 * File:   serverClient.h
 * Author: szczepanczykd
 *
 * Created on April 8, 2016, 8:30 PM
 */

#ifndef SERVERCLIENT_H
#define	SERVERCLIENT_H

#include <iostream>
#include <cstring>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "serverSocket.h"

#include "server.h"

class serverClient : public server {
    
struct sockaddr_in _clientName;
int _client_socket;
int _client_lenght;
    
public:
    serverClient();
    serverClient(serverSocket &server);
    virtual ~serverClient();
    static int numbers;
    int ID;
    struct sockaddr_in getAddrport();
    int& getConnectSocket();
    int& getClientLenght();
    void kill();
private:

};

#endif	/* SERVERCLIENT_H */

