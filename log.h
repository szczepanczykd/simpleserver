/* 
 * File:   log.h
 * Author: szczepanczykd
 *
 * Created on April 9, 2016, 11:09 PM
 */

#ifndef LOG_H
#define	LOG_H

#include "fileRaii.h"
#include <ctime>
#include <fstream>
#include <iostream>

class log {
    fileRaii *__file;
    log(fileRaii &file) { __file = &file; }
    log(const log& orig) { }
public:

    static log & logData(fileRaii &file)
    {
        static log singleton(file);
        return singleton;
    }
    
    template < typename V >
    log& operator << ( const V & out)
    {
                
        time_t t = time(0);
        struct tm * now = localtime( &t);
        if(__file->is_open())
        {
        __file->writeText(now->tm_year + 1900);
        __file->writeText('-');
        __file->writeText(now->tm_mon + 1);
        __file->writeText('-');
        __file->writeText(now->tm_mday);
        __file->writeText(':');
        __file->writeText(out);
        __file->writeText('\n');
        }
        else
        {
        std::ofstream file("FILE_ERROR.txt",std::ios_base::app);
        fileRaii f(file);    
        f.writeText(now->tm_year + 1900);
        f.writeText('-');
        f.writeText(now->tm_mon + 1);
        f.writeText('-');
        f.writeText(now->tm_mday);
        f.writeText('\n');
        f.writeText(" Logged message to not opened file !");
        f.writeText('\n');
        }
        return *this;
    }

};

#endif	/* LOG_H */

