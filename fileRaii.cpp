/* 
 * File:   fileRaii.cpp
 * Author: szczepanczykd
 * 
 * Created on April 9, 2016, 10:46 PM
 */

#include "fileRaii.h"

fileRaii::fileRaii(std::ofstream &file) {
    __file = &file;
}

fileRaii::~fileRaii() {
    __file->close();
}

bool fileRaii::is_open()
{
    return __file->is_open();
}