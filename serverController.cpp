/* 
 * File:   serverController.cpp
 * Author: szczepanczykd
 * 
 * Created on April 10, 2016, 1:53 PM
 */

#include "serverController.h"


serverController::serverController() {
}

serverController::serverController(const serverController& orig) {
}

serverController::~serverController() {
}

int serverController::sendMessage(std::string text,serverClient & client)
{
    write(client.getConnectSocket(),text.c_str(),strlen(text.c_str()));
}

std::string serverController::reciveMessage()
{
    char recived [255] = "";
    int status = read(_client.getConnectSocket(),&recived,300);
    if(status == 0)
    {
        cout << "Nic nie odebrano" <<endl;
        return "";
    }
    
    return recived;
}
    
std::string serverController::parseMessage(std::string text)
{
    if(text.size()>5)
    {
    istringstream iss(text);
    string s1 = "";
    getline(iss,s1,'-');

    if(s1 != "")
    {
        return s1;
    }
    return "";
    }
        
}

void serverController::loopped_task(std::vector <serverClient> *&clients)
{
        
    string text = "";
    _client = clients->at(clients->size()-1);

    sendMessage("Hello!\n",_client);
    while( 1 )
    {
      
        text = reciveMessage();
        if( text  != "")
        {
            cout << text;
            string proto = parseMessage(text);
            if(proto == "")
            {
                kill(clients);
                return;
            }
            
            cout << "Used proto is: " << proto <<endl;
            serverProtoImplem::useProto(proto,clients,&_client,text);
            
            
        }
        else
        {
            kill(clients);
            return;
        }
    }
    
}

void serverController::kill(std::vector <serverClient> *&clients)
{
    for(int i = 0;i<clients->size();i++)
            {
                if(_client.ID == clients->at(i).ID)
                {
                    clients->erase(clients->begin()+i);
                    ofstream file;
                    fileRaii f(file);
                    log::logData(f) << "User Disconnected! ";
                    return;
                }
            }
}