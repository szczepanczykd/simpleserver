/* 
 * File:   main.cpp
 * Author: szczepanczykd
 *
 * Created on April 8, 2016, 6:50 PM
 */


#include <iostream>
#include <cstring>

#include <thread>
#include <vector>
#include "serverParams.h"
#include "getopt.h"
#include "serverSocket.h"
#include "serverClient.h"

#include <signal.h>
#include <fstream>
#include "fileRaii.h"
#include "log.h"
#include "serverProtoImplem.h"
#include "serverController.h"
#include <memory>

using namespace std;

void connectedUserTask(vector <serverClient> *clientList);
serverParams parseOptions(int argc,char ** argv);
void signal_handler(int signumber);

serverSocket server;
vector <std::thread> threadList;
vector <serverClient> clientList;


int main(int argc, char** argv) {

    
    serverParams startupParams;
    startupParams = parseOptions(argc,argv);  
    server.startListening(startupParams.ip,startupParams.port);
      
    if (signal(SIGINT, signal_handler) == SIG_ERR)
    cout << "can't catch SIGINT\n";
          
    ofstream file;
    file.open("log.txt",ios_base::app);
    fileRaii f(file);
    
    auto dirProto = serverProtoImplem::serverProtoFactory("DIR");
    auto allProto = serverProtoImplem::serverProtoFactory("ALL");
    auto getusrsProto = serverProtoImplem::serverProtoFactory("GETUSRS");
    
    serverProtoImplem::addProto(*dirProto);
    serverProtoImplem::addProto(*allProto);
    serverProtoImplem::addProto(*getusrsProto);
    
    while (1)
    {
    //serverClient * client = new serverClient(server);
    unique_ptr <serverClient> client = unique_ptr <serverClient> (new serverClient(server));
    clientList.push_back(*client);
    
   // thread *t1 = new thread(connectedUserTask,&clientList);
    //t1->detach();
    
    unique_ptr <thread> t1 = unique_ptr <thread> (new thread(connectedUserTask,&clientList));
    
    threadList.push_back(move(*t1));// 1) 
    
    log::logData(f)<< "!!!\nUser Connected" << "IP is: " << inet_ntoa(client->getAddrport().sin_addr) << "___\n";
    
    //  delete t1;
    //  delete client;
    //they will die here.
    }
       
    
    return 0;
}

void connectedUserTask(vector <serverClient> *clientList)
{
    auto controller = shared_ptr <serverController> (new serverController);
    controller.get()->loopped_task(clientList);
}

serverParams parseOptions(int argc,char ** argv)
{
    serverParams params = { "127.0.0.1", 5535 };
    bool port = false;
    int a;
    const char * short_opt = "d?i:p:";
    struct option long_opt[] =
    {
        {"ip", required_argument, NULL,'i'},
        {"port", required_argument, NULL,'p'},
         {"help", no_argument, NULL,'?'},
        {"daemon", no_argument, NULL,'d'},
    };
    
    while ( (a = getopt_long(argc,argv,short_opt,long_opt,NULL)) != -1)
    {
        switch(a)
        {
            case 'i':
                params.ip = optarg;
            break;
            case 'p':
                params.port = atoi(optarg);
                port = true;
            break;
            case 'd':
                daemon(1,0);
            break;
            case '?':
            std::cout << "Usage: simpleserver -i(--ip) <interface IP> | -p(--port) <port number> | -d (--daemon) run into daemon mode |-?(--help) \n";
            exit(0);
            break;              
                        
        };
    }
    if(!port)
    {
        std::cout << "Port have not been seted! Default is 5535 " << endl;
    }
   return params; 
}

void signal_handler(int signumber)
{
    if( signumber == SIGINT)
    {
    clientList.clear();
    threadList.clear();
    server.kill();
    exit(1);
    }
}