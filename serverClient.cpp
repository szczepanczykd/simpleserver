/* 
 * File:   serverClient.cpp
 * Author: szczepanczykd
 * 
 * Created on April 8, 2016, 8:30 PM
 */

#include "serverClient.h"

int serverClient::numbers = 1;

serverClient::serverClient()
{
     ID = 0;
    _client_socket = 0;
    _client_lenght = 0; 
}

serverClient::serverClient(serverSocket &server) {
    
    
    ID = numbers;
    numbers++;
    cout << "CLIENT ID: " << ID <<" State (AWAITING)\n";
    
    _client_socket = 0;
    _client_lenght = sizeof(_clientName);
    //awaiting there!
    _client_socket = accept(server.getConnectSocket(), (struct sockaddr *) &_clientName,(socklen_t *) &_client_lenght);
     
    if(_client_socket == -1)
    {
    cout << "Cannot accept connection\n";
    close(server.getConnectSocket());
    }
    
}



serverClient::~serverClient() {
    ID = 0;
    _client_socket = 0;
    _client_lenght = 0;
}

struct sockaddr_in serverClient::getAddrport()
{
    return _clientName;
}
int& serverClient::getConnectSocket()
{
    return _client_socket; 
}
int& serverClient::getClientLenght()
{
    return _client_lenght;
}
    
void serverClient::kill()
{
     ID = 0;
    _client_socket = 0;
    _client_lenght = 0;
}