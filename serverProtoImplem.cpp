/* 
 * File:   serverProtoImplem.cpp
 * Author: szczepanczykd
 * 
 * Created on April 10, 2016, 7:49 PM
 */

#include "serverProtoImplem.h"

vector <serverProto*> serverProtoImplem::_proto_list;

void serverProtoImplem::useProto(string protoName,std::vector <serverClient> *&clients, serverClient *sender, const std::string message) {
    for(int i = 0; i < serverProtoImplem::_proto_list.size();i++)
    {
        if( serverProtoImplem::_proto_list.at(i)->getProtoName() == protoName)
        {
            cout << "Proto founded! " <<endl;
            serverProtoImplem::_proto_list.at(i)->sendProtoMessage(clients,sender,message);
            break;
        }
    }
}

void serverProtoImplem::addProto(serverProto &proto) {

    serverProtoImplem::_proto_list.push_back(&proto);
}

void serverProtoDir::sendProtoMessage(std::vector <serverClient> *&clients, serverClient *sender, const std::string message) {

    ostringstream ss;
    ss << sender->ID;
    string ID = ss.str();
    
    
    istringstream iss(message);
    string helper;
    
    getline(iss,helper,'-');
    getline(iss,helper,'-');
    int destination_id = atoi(helper.c_str());
    getline(iss,helper,'\n');
    
    string msg = "DIR-" + ID + "-" + helper + '\n';

 
    
    for(int i = 0; i<clients->size();i++)
    {
        if(destination_id == clients->at(i).ID)
        {
            cout << "Found ID! "<<endl;
         serverController::sendMessage(msg,clients->at(i));
         break;
        }
        
    }
    cout << "Function Doned!" <<endl;
    ss.clear();
}

string serverProtoDir::getProtoName() {
    return _protoName;
}

void serverProtoAll::sendProtoMessage(std::vector <serverClient> *&clients, serverClient *sender, const std::string message) {
    ostringstream ss;
    ss << sender->ID;
    string ID = ss.str();
    
    istringstream iss(message);
    string helper;
    
    getline(iss,helper,'-');
    getline(iss,helper,'\n');
    
    string msg = "ALL-" + ID + "-" + helper  + '\n';
    
    for(int i = 0; i< clients->size();i++)
    {
        if(clients->at(i).ID != sender->ID)
        {
            serverController::sendMessage(msg,clients->at(i));
        }
    }
    
}

string serverProtoAll::getProtoName() {
return _protoName;
}

void serverProtoGetusrs::sendProtoMessage(std::vector <serverClient> *&clients, serverClient *sender, const std::string message) {

    ostringstream ss;
    
    for(int i = 0; i< clients->size();i++)
    {
        ss << "-" << clients->at(i).ID;
    }
    string msg = "GETUSRS" + ss.str()  + '\n';
    cout << "getusrs: " <<msg <<endl;
    serverController::sendMessage(msg,*sender);
}

string serverProtoGetusrs::getProtoName() {
return _protoName;
}


serverProtoDir::serverProtoDir(string protoName)
{
    _protoName = protoName;
}

serverProtoAll::serverProtoAll(string protoName)
{
    _protoName = protoName;
}

serverProtoGetusrs::serverProtoGetusrs(string protoName)
{
    _protoName = protoName;
}

auto serverProtoImplem::serverProtoFactory(string protoName) -> unique_ptr <serverProto>
{
    if(protoName == "DIR")
        return unique_ptr <serverProto>(new serverProtoDir(protoName));
    else if(protoName == "ALL")
        return unique_ptr <serverProto>(new serverProtoAll(protoName));
    else if(protoName == "GETUSRS")
        return unique_ptr <serverProto> (new serverProtoGetusrs(protoName));
    else
        return 0;
    
}